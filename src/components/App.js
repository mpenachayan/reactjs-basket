import React, { Component } from 'react';
import '../css/App.css';
import * as Constants from '../utils/constants';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      competitions: []
    };
  }

  componentDidMount() {
    fetch(Constants.COMPETITIONS_ENDPOINT)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            competitions: result.competitions
          });

        },

        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }


  render() {

    let ligaEndesaArray = this.state.competitions.filter((item) => {
      return Constants.KEY_LIGA_ENDESA === item.competitionAbbrev;
    });

    let currentWeekId = null;
    if (ligaEndesaArray.length > 0) {
      currentWeekId = ligaEndesaArray[0].currentEdition.currentWeekId
      console.log(currentWeekId);
    }
    if (currentWeekId != null) {
      fetch(Constants.WEEK_MATCHES_ENDPOINT.replace('{{weekId}}', Number(currentWeekId))).then(res => res.json())
        .then(
          (result) => {
            console.log(result);
          },
          (error) => {
            console.log(error);
          }
        );
    }
    return (
      <div>
        {currentWeekId}
      </div>
    );
  }

}

export default App;