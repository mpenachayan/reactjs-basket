export const COMPETITIONS_ENDPOINT = 'http://openapi.acb.com/v1/competitions';
export const WEEK_MATCHES_ENDPOINT = 'http://openapi.acb.com/v1/weeks/{{weekId}}';

export const KEY_LIGA_ENDESA = 'LACB';
export const KEY_COPA = 'CREY';
export const KEY_SUPERCOPA = 'SCOPA';